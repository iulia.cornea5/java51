public class ForInstruction {

    /*
    * * * * * * * * * * * * * * * * * * * * * * * * * ....* * * * *
    1 2 3 4 5 6 7 8                                         ... 100
     */

    public static void main(String[] args) {
        int i;
        // 1 -> 100, pas = 1
        // a) o singură dată, la început, i este inițializat cu 1
        // a.2) se verifică condiția de oprire
        for (i = 1; i <= 100; i++) {
            // b) se intră în corpul/blockul lui for
            System.out.print("*");
            System.out.print(" ");
        }
        // c) se execută pasul de incrementare
        System.out.println("^");

        // modificare pas (i crește cu 2)
        for (i = 0; i <= 50; i += 2) {
            System.out.println(i + " ");
        }

        // modificare direcție
        for (i = 10; i >= 0; i--) {
            System.out.println(i + " ");
        }

        // suma de la 2 la 78 = 2 + 3 + 4 + 5 + ..... + 78
        int suma = 0;
        for (i = 2; i <= 78; i++) {
            suma = suma + i;
            int varDinFor = 0;
        }

//        System.out.println(varDinFor); nu e vizibila aici



    }

}
