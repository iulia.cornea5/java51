public class IfInstruction {

    public static void main(String[] args) {
        int a = 7, b = 22;
        a = b; // a nu va mai fi 7, ci a va fi 22; citim: "a ia valoarea b"

        if (a == b) // verifică dacă a este egal cu b; citim: "a este egal cu b"
        {
            System.out.println("a este egal cu b");
        } else {
            System.out.println("a nu este egal cu b");
        }

        if (a != b) // verifică dacă a este diferit de b
        {
            System.out.println("a este diferit de b");
        } else {
            System.out.println("a este egal cu b");
        }

        int var = 11;
        // se execută tot ce e între acolade {} după "if (...)" când condiția e adevăratată.
        if (var >= 10) {
            int dublul = 2 * var;
            System.out.println("variabila mai mare ca 10 iar dublul ei este " + dublul);
        }

        // se execută doar prima instrucțiunea după "if (...)" când condiția e adevăratată. aici lipsesc acoladele
        int jumatate = -1;
        if (var <= 10)
            jumatate = var / 2;
        System.out.println("variabila e mai mică decât 10 iar jumătatea ei este " + jumatate);

        // Exercițiul 1
        // calculați dacă elevul are medie de trecere
        int nota1 = 4, nota2 = 4, nota3 = 5, nota4 = 5;
        double media = (((double) nota1) + nota2 + nota3 + nota4) / 4;
        // afișați mesajul "Materie promovată"  în cazul în care media e >= 4.5 sau "Necesită mărire de notă" în caz contrar
        if (media > 4.5) {
            System.out.println("Materie promovată");
        } else {
            System.out.println("Necesită mărire de notă");
        }
        // adițional dacă media este 10 afișați "Necesită bursă"
        if (media == 10) {
            System.out.println("Necesită bursă");
        }

        // Exercițiul 2
        // se dau două numere pe care le considerăm laturile unei camere.
        // daca CEL PUȚIN una dintre laturile camerei e mai mare de 3m ȘI
        // suprafata camerei este mai mare de 9m2 atunci afișăm "E o cameră", altfel afișma "E un birou"
        double latura1 = 3, latura2 = 4;
        double aria = latura1 * latura2;
        boolean latura1Ok = latura1 > 3;
        boolean latura2Ok = latura2 > 3;
        boolean ariaOk = aria >= 9;

        if ((latura1Ok || latura2Ok) && ariaOk) {
            System.out.println("E o camera");
        } else {
            System.out.println("E un birou");
        }

        double vopseaDisponibila = 15;
        double inaltime = 2.5;

        // Exemplu de if în if
        if ((latura1 > 3 || latura2 > 3) && aria > 9) {
            System.out.print("E o cameră");
            double vopseaNecesara = (latura1 + latura2 + latura1 + latura2) * inaltime + latura1 * latura2;
            if (vopseaDisponibila >= vopseaNecesara) {
                System.out.println(" și avem suficientă vopsea să o vopsim.");
            } else {
                System.out.println(" dar nu avem suficienta vopsea sa o vopsim.");
            }
        } else {
            System.out.println("E un birou.");
        }

        // Exemplu if else if
        // if ....... if else .......  if else if
        // < 6 m2 nu poate fi decat baie
        // 6 - 9 m2 birou
        // >9 m2 e o camera

        if (aria > 9) {
            System.out.println("Camera");
        } else if (aria > 6) {
            System.out.println("Birou");
        } else {
            System.out.println("Baie");
        }

        // Exercițiul 3
        // un barbat vrea sa mearga in club.
        // ca sa intre in club trebuie sa fie major (>=18).
        // Ca sa poata comanda alcool trebuie sa aibe cel putin 21 de ani.

        // ??? poate intra in club si poate sa bea,  poate intra in club dar nu poate bea, nu poate intra in club


        // are X bani, si o sampanie costă Y bani.
        // ?? poate intra in club si poate sa bea sampanie
        // poate intra in club si poate sa bea dar nu sampanie


    }
}
