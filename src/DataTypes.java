public class DataTypes {

    public static void main(String[] args) {
        // am declarat variabila "x", care are tipul de date "int"
        int x;
        // am asignat valoarea "5" variabilei "x"
        x = 5;
        int a_1 = 13, b_2 = 14;
        x = a_1 + b_2;
        x = x + 1;

        // afișăm textul ditnre ghilimele
        System.out.print("x = ");
        // afișăm valoarea variabilei x
        System.out.println(x);

        short shortInteger = 125;


        long max = 5000000000l;
        max = max + max;
        System.out.print("max = ");
        System.out.println(max);

        System.out.println("max = " + max);

        // declarare cu asignare de variabile de tipul double
        double a = 14.5, b = 467.7, c = 123;
        // printează mot-a-mot ce este între ghilimele
        System.out.println("a + b");
        // efectuează calculul și printează rezultatul
        System.out.println(a + b);

        // decalarare și asignare variabilă de tipul float
        float ff = 4.453453437462378468236482764832f;


        // declarare și asignare variabile de tipul boolean
        boolean m = true, n = false;
        boolean result;
        // operatia SI
        // m AND n adevărat doar când ambele sunt adevărate
        result = m && n;
        System.out.print("m && n = ");
        System.out.println(result);

        // operatia SAU
        // m OR n adevărat când cel puțin una e adevărată
        result = m || n;
        System.out.print("m || n = ");
        System.out.println(result);

        // exemplu de asignare valori
        x = 5;
        x = 44;
        n = true;
        n = false;

        boolean v;
        // boolean v; <- incorect! NU pot avea două variabile cu același nume
        // boolean v = 44, v=66; <- incorect! NU pot avea două variabile cu același nume

        char c1 = 'g';
        System.out.println(c1);

        String cuvant = "pere";
        String propzitie = "Salutare, vrei pere?";
        // propzitie = false; <- incorect, asignarea nu funcționează
        System.out.println(propzitie);
        propzitie = "Altă valoare pentru propozitie.";
        System.out.println(propzitie);

        propzitie = "Salut! " + "Eu sunt Marcel! ";
        System.out.println(propzitie);
        propzitie = propzitie + " Pe tine cum te cheamă?";
        System.out.println(propzitie);
        System.out.println("Pe mine " + "mă cheamă " + "Anton.");


        int p, j;
        p = 14;
        j = 12;
        int suma = p + j;
        System.out.print("p + j = ");
        System.out.println(suma);
        System.out.println("p + j = " + suma); // string + int <- consideră plusul ca alipire => afișează: p + j = 26
        System.out.println(p + j); // int + int <- consideră plusul ca adunare => afișează: 26
        System.out.println("p + j = " + p + j); // string + int + int <- consideră primul plus ca alipire =>  "p + j = 14" (string) + j (int) => afișează: p + j = 1412
        System.out.println("p + j = " + (p + j)); // execută adunarea din paranteze prima dată



    }

}
