public class Regex {

    public static void main(String[] args) {
        String s1 = "Ioana-Maria";
        System.out.println(s1.matches("[a-zA-Z-]+"));

        String cnp = "1234567891234";
        System.out.println(cnp.matches("[0-9]{13}"));

    }
}
