package oop;

import oop.school.Student;

public class Main {
    public static void main(String[] args) {

        Student ana = new Student("Ana", 12, 9.2);
        System.out.println(ana.introduceYourself());


        Student banana = new Student("Banana", 17, 8.7);
        System.out.println(banana.introduceYourself());


//        System.out.println(nume); nu e încă declarată
        greet("Maria");
        String nume = "Ion";
        greet(nume);

        //declarare tipul de date - numele variabilei
        Student s1;
        // asignat o valoare
        s1 = new Student("Marcela", 35, 8.689);
        System.out.println("Numele lui s1:" + s1.nume);

        // apelare metoda din clasa Student
//        System.out.println("Marcela" + s1.hasScholarship());
//        System.out.println(s1.nume + s1.hasScholarship());
        System.out.println(s1.hasScholarship());


        // hasScholarship(); nu putem apela astfel deoarece are nevoie de un obiect de tipul student

        String ceva;
        ceva = "inițializat";

        Masina m1;
        m1 = new Masina();
        m1.marca = "Audi";
        m1.model = "A4";
        m1.anulFabricatiei = 2000;
        m1.capacitateCilindrică = 2.7;
        // Audi A4 din 2000 cu capacitate cilindrica de 2.7
        System.out.println(
                m1.marca + " " + m1.model + " din " + m1.anulFabricatiei
                        + " cu capacitate cilindrica de " + m1.capacitateCilindrică
        );

        goodbye();
    }


    static void greet(String personName) {
        System.out.println("Hello there, " + personName + "!");
    }

    static void goodbye() {
//        System.out.println(nume); nu e vizibila pentru ca e declarata in alta metoda, (e declarata in metoda main)
        System.out.println("Bye bye for now!");
    }


}
