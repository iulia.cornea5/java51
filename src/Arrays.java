import java.util.Scanner;

public class Arrays {

    public static void main(String[] args) {
        int[] sir = {0, 1, 2, 3, 4, 5};
        System.out.println(sir[0]);

        boolean[] boolArray = {true, true, false};

        String[] stringArray = new String[]{"ala", "bala", "portocala"};
        System.out.println(stringArray[2]);
        for (int i = 0; i < stringArray.length; i++) {
            System.out.println(stringArray[i]);
        }

        String[] unintializedStrinArray = new String[10];
        unintializedStrinArray[7] = "Sapte";

        System.out.println(unintializedStrinArray[7]);
        for (int i = 0; i < 10; i++) {
            System.out.print(unintializedStrinArray[i] + " ");
        }
        System.out.println();

        // citi si stocati in memorie 10 numere
        // calculati apoi suma lor
        // media aritmetica
        // pentru fiecare numar afisati câte cifre are


        // 89 56 34 ... 568
        Scanner keyboard = new Scanner(System.in);
        int numere[] = new int[10];
        for (int i = 0; i < 10; i++) {
            numere[i] = keyboard.nextInt();
        }

        // i e indicele 0,1,2,....10
        // numere[i] 89, 56, 34 ...568
        for (int i = 0; i < 10; i++) {
            afiseazaUltimeleDouaCifre(numere[i]);
        }

    }

    private static void afiseazaUltimeleDouaCifre(int nr) {
        System.out.println(nr % 100);
    }
}
